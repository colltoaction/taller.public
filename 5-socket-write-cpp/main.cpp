#include <string>
#include <cstring>
#include <cstdio>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>


class Socket
{
private:
	int skt;
	struct addrinfo& res;
public:
	explicit Socket(struct addrinfo& res) : res(res) {
		skt = socket(
			this->res.ai_family,
			this->res.ai_socktype,
			this->res.ai_protocol);
		connect(skt, res.ai_addr, res.ai_addrlen);
	}
	
	~Socket(){
		shutdown(skt, SHUT_RDWR);
		close(skt);
	}

	void send(void const* msg, size_t length) {
		::send(skt, msg, length, MSG_NOSIGNAL);
	}
};

class ServerProxy
{
private:
	struct addrinfo* res;
	Socket* skt;
public:
	ServerProxy(const char* hostname, const char* port) {
		struct addrinfo hints;

		std::memset(&hints, 0, sizeof(hints));
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_family = AF_INET;

		getaddrinfo(hostname, port, &hints, &res);
		skt = new Socket(*res);
		freeaddrinfo(res);
	}

	~ServerProxy() {
		delete skt;
	}

	void send(std::string const& msg) {
		skt->send(msg.c_str(), msg.length());
	}
};


int main(int argc, char const *argv[])
{
	if (argc != 3) {
		printf("Usage: ./main <hostname> <port>\n");
		return 1;
	}

	const char *hostname = argv[1];
	const char *port = argv[2];

	ServerProxy server(hostname, port);
	server.send("hola mundo\nmama\n");

	return 0;
}
