# Send through socket with TCP example in C++

Basic send example with a TCP socket in C++. Uses netcat to listen and writes the data to the `__received__` file.

Verifies cpplint, runs with valgrind, checks the expected output.

## How to run:

1) if using docker:

	./run.sh

2) if running locally:

	make
