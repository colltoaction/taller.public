# Send through socket with TCP example

Basic send example with a TCP socket. Uses netcat to listen and writes the data to the `received.txt` file.

Verifies cpplint and runs with valgrind.

## How to run:

1) if using docker:

	./run.sh

2) if running locally:

	make
