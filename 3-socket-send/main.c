/***
	TODO: reutilizar código de manejo de errores
***/


#include <stdio.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
	if (argc != 3)
	{
		printf("Usage: ./main <hostname> <port>\n");
		return 1;
	}

	const char *hostname = argv[1];
	const char *port = argv[2];
	struct addrinfo hints, *res;
	int err;

	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = AF_INET;

	if ((err = getaddrinfo(hostname, port, &hints, &res)) != 0) {
        printf("getaddrinfo(): %s\n", gai_strerror(err));
		return 1;
	}

	int skt = socket(
		res->ai_family,
		res->ai_socktype,
		res->ai_protocol
	);

	if (connect(skt, res->ai_addr, res->ai_addrlen) != 0) {
		perror("connect()");
		shutdown(skt, SHUT_RDWR);
		close(skt);
		freeaddrinfo(res);
		return 1;
	}

	char* buf = "hola mundo";
	if (send(skt, buf, strlen(buf), MSG_NOSIGNAL) < 0) {
		perror("send()");
		shutdown(skt, SHUT_RDWR);
		close(skt);
		freeaddrinfo(res);
		return 1;
	}

	shutdown(skt, SHUT_RDWR);
	close(skt);
	freeaddrinfo(res);

	return 0;
}
